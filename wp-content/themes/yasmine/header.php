<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="<?php  echo get_template_directory_uri() . '/fonts/baskerville/baskerville.css' ?>">
  <link rel="stylesheet" href="<?php  echo get_template_directory_uri() . '/fonts/elsie/elsie.css' ?>">
  <link rel="stylesheet" href="<?php  echo get_template_directory_uri() . '/fonts/allura/allura.css' ?>">
  <link rel="stylesheet" href="<?php  echo get_template_directory_uri() . '/style.css' ?>"> 
  <link rel="stylesheet" href="<?php  echo get_template_directory_uri() .'/leaflet/leaflet.css'?>" /> 
  <script src="<?php  echo get_template_directory_uri() .'/leaflet/leaflet.js'?>"></script> 
    <script type='text/javascript' src="<?php echo get_template_directory_uri() .'/js/script.js' ?>"></script>
  <?php wp_head() ?>
  
  <title>Yasmine & Greg</title>
</head>
<body>
  <main>
    <!-- menu 2 -->
    <!-- <nav class="site-nav">

<div class="menu-toggle">
  <div class="hamburger"></div>
</div>

<ul class="open desktop">
  <li><a href="#!">Home</a></li>
  <li><a href="#!">Latest News</a></li>
  <li><a href="#!">About us</a></li>
  <li><a href="#!">Download Plugin</a></li>
  <li><a href="#!">Contact us</a></li>
  
</ul>
</nav> -->
       <!-- menu 1 -->
        <nav class= "nav">
          <div class="menu-toggle">
            <div class="hamburger">
            </div>
          </div>
          <ul >
            <li class = "nav__left">
              <a href="#rsvp">RSVP</a>
            </li>
            <li class = "nav__left" >
              <a href= "#directions" >Where & When</a>
            </li>
            
            <li>
            <a href="#first-screen" class = "nav__logo">Yasmine <br>& Greg</a>
              
            </li>
            <li class = "nav__right" >
              <a href="#hotels">Accomodation</a>
            </li>
              
            
            <li class = "nav__right">
              <a href="#wed-list">Wedding List</a> 
            </li>
          </ul>
        </nav>
        