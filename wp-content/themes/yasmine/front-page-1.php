<?php get_header() ?>

      
      <section id = "first-screen" class="first-screen u-container-text-padding">
        <style>
          .first-screen {
          background-image: url("<?php echo get_template_directory_uri() . '/img/south-africa.png' ?>");
          background-size: cover;
          
          filter:  contrast(.7);
        }
        @media screen and (max-width: 415px) {
          .first-screen {
          background-image: url("<?php echo get_template_directory_uri() . '/img/south-africa-responsive.jpg' ?>");
          background-size: cover;
          
          filter:  contrast(.7);
        }

        }

         </style>
        <h1 class="first-screen__title">
          <span class="first-screen__title--3">
            <span class="first-screen__title--3-1">Vullierens,</span>
            <span class="first-screen__title--3-2">July 4th 2020 </span>
          </span> <br>
          <span class="first-screen__title--2">
            <span class="first-screen__title--2-1">-</span> 
            <span class="first-screen__title--2-2">Greg </span> 
            <span class="first-screen__title--2-3">& Yas </span>
            <span class="first-screen__title--2-1">-</span> 
          </span>
        
        </h1>
        
      </section>
      <section id = "intro" class = "intro u-container-text-padding-large">
        <div class="intro__text u-center-text">
          <h2 class="intro__title">Our wedding</h2>
          <p class="intro__paragraph">
            Welcome to our wedding website, where you will find all relevant information. Please send your <span class="strong">RSVP below by May 15th</span>. 
            We sincerely hope you can make it, we can’t wait to share our special day with you and celebrate!!
          </p>
        </div>
      </section>
      
      <section id = "directions" class="directions">
        <div class="directions__map">
        <div id="map"></div>
    <script>
      // initialize Leaflet
      var map = L.map('map').setView({lon: 6.479421, lat:   46.5707107}, 14);
      map.scrollWheelZoom.disable()
      // add the OpenStreetMap tiles
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
      }).addTo(map);

      // show the scale bar on the lower left corner
      L.control.scale().addTo(map);

      // show a marker on the map
      L.marker({lon: 6.479421, lat: 46.5707107}).bindPopup('The center of the world').addTo(map);
    </script>
   
        </div>
        <div class="directions__text u-container-text-padding">
          <h2 class="directions__header">Where & When</h2>
          <p class="directions__content">
          The wedding ritual, along with the dinner and celebration, will take place at <span class="strong">17:00 sharp </span>on <span class="strong">July 4th 2020</span> at the following location:
          </p>
          <p>
            <address>
              Portes des Iris<br>
              Route de Cottens<br>
              CH-1115 Vullierens s/Morges<br>
              Switzerland
            </address>
          </p>
          
          <p class="directions__content">
            For our dear guests joining us from abroad: the closest airport is Geneva airport, which is 45 minutes driving distance to our wedding location.
          </p> 
          
          
        </div>
      </section>
      <section id = "hotels" class="hotels u-container-text-padding">
      <h2 class = "hotels__title">Accommodation</h2>
      <p class = "hotels__text">
        For those who would like to stay at a hotel we have a selection of hotels in the Ouchy area, which is located 25min from the venue. 
      </p>
      <p class = "hotels__text">
      <span class="strong">Shuttle bus:</span> We have organised transportation from Château d’Ouchy to the venue location, it will leave at <span class="strong">16:00 sharp</span>. As for the way back to hotels in Ouchy, some shuttle buses will be organised.
      </p>
        <div class="hotels__flex">
          <div class="hotels__hotel">
            <h3>Beau-Rivage Palace</h3>
            
            <p class="hotels__price-range"><span>Price range:<br></span> CHF 360 - CHF 600</p>
            <p class="hotels__location"><span>Location:<br></span> Lausanne</p>
            <p class="hotels__location"><span>Phone number:<br></span> +41 21 613 33 33</p>
            
            <a href="https://brp.ch" class="btn">To hotel</a>
          </div>


          <div class="hotels__hotel">
            <h3>Angleterre & Résidence</h3>
            
            <p class="hotels__price-range"><span>Price range:<br></span> CHF 250 - CHF 400</p>
            <p class="hotels__location"><span>Location:<br></span> Lausanne</p>
            <p class="hotels__location"><span>Phone number:<br></span> +41 21 613 34 40</p>
            
            <a href="https://www.angleterre-residence.ch" class="btn">To hotel</a>
          </div>
          <div class="hotels__hotel">
            <h3>Château d'Ouchy</h3>
            <p class="hotels__price-range"><span>Price range:<br></span> CHF 250 - CHF 400</p>
            <p class="hotels__location"><span>Location:<br></span> Lausanne</p>
            <p class="hotels__location"><span>Phone number:<br></span> +41 021 331 51 42</p>
            
            <a href="https://www.chateaudouchy.ch" class="btn">To hotel</a>
          </div>

          <div class="hotels__hotel">
            <h3>Mövenpick Hotel Lausanne</h3>
            <p class="hotels__price-range"><span>Price range:<br></span> CHF 150 - CHF 200</p>
            <p class="hotels__location"><span>Location:<br></span> Lausanne</p>
            <p class="hotels__location"><span>Phone number:<br></span> +41 021 612 76 12</p>
            
            <a href="https://www.movenpick.com/europe/switzerland/lausanne/hotel-lausanne/overview/" class="btn">To hotel</a>
          </div>
          <div class="hotels__hotel">
            <h3>Hôtel du Port</h3>
            <p class="hotels__price-range"><span>Price range:<br></span> CHF 120 - CHF 180</p>
            <p class="hotels__location"><span>Location:<br></span> Ouchy</p>
            <p class="hotels__location"><span>Phone number:<br></span> + 41 21 612 04 44</p>
            
            <a href="http://www.hotel-du-port.ch/" class="btn">To hotel</a>
          </div>
          </div>
      </section>
      <section id = "wed-list" class="wed-list">
        
        <div class="wed-list__image">
        <style>
          .wed-list__image {
          background-image: url("<?php echo get_template_directory_uri() . '/img/romantic-sunset-2.png' ?>");
          background-size: cover;
          filter:  contrast(.7);
        }
         </style>
        </div>
        <div class="wed-list__text u-container-text-padding">
          <h2 class="wed-list__title">Wedding List</h2>
          <p class="wed-list__content">
          There is nothing bringing us more joy than you making it all the way to our wedding, and celebrate with us. However, should you wish to honour us with a gift, a contribution towards our honeymoon would be appreciated.
          </p>
          
          <a href="https://gregandyas.zankyou.com/en" class="btn wed-list__iban-btn">Contribute</a>
          
        </div>
        
      </section>
      
      <section id = "story" class="story u-center-text">
        
        <div class="story__text u-container-text-padding">
          <h2 class="story__title">We're looking forward to celebrating with you!</h2>
          
          
        </div>
        <div class="story__image">
        <style>
          .story__image {
          background-image: url("<?php echo get_template_directory_uri() . '/img/snow-kiss.jpg' ?>");
          background-size: cover;
          filter:  contrast(.6);
        }
         </style>
        </div>
      </section>
      <section id = "rsvp" class="rsvp">
      <style>
          .rsvp {
          background-image: url("<?php echo get_template_directory_uri() . '/img/forest-hiking-2.jpg' ?>");
          background-size: cover;
        }
        @media screen and (max-width: 415px) {
          .rsvp {
            background-image: url("<?php echo get_template_directory_uri() . '/img/forest-hiking-responsive.jpg' ?>");
            background-size: cover;
        }
         </style>
        <div class="form u-container-text-padding">
          <h2>RSVP</h2>
          <?php echo do_shortcode( '[contact-form-7 id="7" title="Contact form 1"]' ); ?>
        </div>
        
      </section>
      
      

  <?php get_footer() ?>