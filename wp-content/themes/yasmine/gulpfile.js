var paths = {
	url: "http://localhost:8888/yasmine/",
  styles: {
      src: "scss/**/*.scss",
      dest: ".",
  }
};

var gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
    browserSync = require("browser-sync").create();

function style() {
	
    return (
	
        gulp	
            .src(paths.styles.src)
            .pipe(sourcemaps.init())
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(postcss([autoprefixer(), cssnano()]))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(paths.styles.dest))
            .pipe(browserSync.stream())
    );
	
}
	
function reload() {
    browserSync.reload();
}

function watch() {
    browserSync.init({
          proxy: paths.url,
    });
    gulp.watch(paths.styles.src, style) ;
    gulp.watch('./*.php').on('change', browserSync.reload);
    gulp.watch('./js/**/*.js').on('change', browserSync.reload);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./**/*.png').on('change', browserSync.reload);
    gulp.watch('./**/*.jpg').on('change', browserSync.reload);
    gulp.watch('./**/*.svg').on('change', browserSync.reload);
}
exports.style = style;
exports.watch = watch;