<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'yasmine' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K**Y3#AbrtE_r9-@0{woMpB6}*<vcLfb+(7qhv&:uPb?>ymi3Pwr3+-(]T5#(rs(' );
define( 'SECURE_AUTH_KEY',  'H1y>qhsV?J?EkKFiER$|Q${Tvb-jn>c}P9 K[<nt>.Q9-*nIII/x`*-M8KWoW-k?' );
define( 'LOGGED_IN_KEY',    ' !~d$B Pj+2.zEp oF02K`yFy;]{4L2qG(Ayfx(]1s%,,MVr=cF++K.oxMYDX!^A' );
define( 'NONCE_KEY',        'L2rr,We*+ `5ktMr)+vOerjV)Y ].W`[[S3CRlQ#<4h_-+5GH%MB|@ w,Br:=Z6f' );
define( 'AUTH_SALT',        'R4gfrNKW;QF.I,tKNV||tO7P&52s`eRR9L&q[WxsebwgBC%4)mTo#xI%h2N yu+X' );
define( 'SECURE_AUTH_SALT', 'Y|j%oJ2ZD5f!Z7n|[(j1uiQ9O7r;C?u>}rMCzTJm.)%uygZ|)SYD4]FAG`!WY!wq' );
define( 'LOGGED_IN_SALT',   '`04o~aU|,<VHl.KbEjz=INB1lwrvQ]Eh_,|uY#jrM4dwo%&>F{$tKyuYRXCfd_zu' );
define( 'NONCE_SALT',       '@nN&3?{LIv%$_^Z>Fc2oe>m5XZ.zoZb,1>NmKk2u@u;uR & /cY%j2ov,.@Z)4XP' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
